import React, { Component } from 'react';

export class InputTextComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hasErrors: null
        };

        this.handleChange = this.handleChange.bind(this); 
    }

    handleChange(event) {// component own event handler
        console.log('InputTextComponent '+ this.props.name +' value changed');

        let hasError = null;// Error flag
        const inputValue = event.target.value;

        this.props.validators.forEach(validator => {// Check input with every validator
            if (!validator(inputValue)) {// if any validator returned FALSE
                hasError = 'invalid';      
            }
        });

        this.setState({
            hasErrors: hasError// sets it's own error state
        });

        this.props.onChange(this.props.name, inputValue);// calls parent event handler with component own name and inputValue
    }

    render() {
        let errorMessage = '';

        if (this.state.hasErrors) {
            errorMessage = <label className="error-message">{this.state.hasErrors}</label>
        }

        return (
        <div>
            <label>
                {this.props.label}:
                <input type="text" value={this.props.value} onChange={this.handleChange} />       
            </label><br></br>
            {errorMessage}
        </div>
        );
    }
  }