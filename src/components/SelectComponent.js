import React, { Component } from 'react';

export class SelectComponent extends Component {
    constructor(props) {
        super(props);
        
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        console.log('InputTextComponent '+ this.props.name +' value changed');

        const inputValue = event.target.value;

        this.props.onChange(this.props.name, inputValue);// calls parent event handler with component own name and inputValue
    }

    render() {
        return (
            <div>
                <label>
                {this.props.label}:
                <select value={this.props.value} onChange={this.handleChange}>
                    <option value="Ukraine">Ukraine</option>
                    <option value="Russia">Russia</option>
                    <option value="Mongolia">Mongolia</option>
                </select>
                </label>
            </div>
        );
    }
}