import React, { Component } from 'react';

import { InputTextComponent } from './InputTextComponent';
import { TextareaComponent } from './TextareaComponent';
import { SelectComponent } from './SelectComponent';

import { isEmail, isPassword } from '../shared/validators';

export class FormComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            emailField: '',
            passwordField: '',
            fullDescriptionField: '',
            countryField: 'Mongolia',
        };
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleFieldChange(name, newValue){   

        this.setState((prevState) => {
            let newState = prevState;
            newState[name] = newValue;
            return newState;
        });
     
    }

    handleSubmit(event) {
        console.log('Form submited !!!');

        const formContent = this.state;

        console.log(JSON.stringify(formContent));
        alert(JSON.stringify(formContent));
    }


    render() {
        return (
            <div className="Form-block">
            <fieldset>
            <legend>{this.props.title}</legend>
                <form onSubmit={this.handleSubmit}>
                    <InputTextComponent className="App-intro" 
                        name="emailField"
                        label="Email" 
                        value={this.state.emailField}
                        onChange={this.handleFieldChange}
                        validators={[isEmail]}/>
                    
                    <InputTextComponent className="App-intro" 
                        name="passwordField"
                        label="Password" 
                        value={this.state.passwordField}
                        onChange={this.handleFieldChange}
                        validators={[isPassword]}/>

                    <TextareaComponent className="App-intro" 
                        name="fullDescriptionField"
                        label="Write about Yourself" 
                        value={this.state.fullDescriptionField}
                        onChange={this.handleFieldChange}/>

                    <SelectComponent className="App-intro" 
                        name="countryField"
                        label="Choose your country" 
                        value={this.state.countryField}
                        onChange={this.handleFieldChange}/>

                    <input type="submit" value="Submit" />
                </form>
            </fieldset>
            </div>
        );
    }
}