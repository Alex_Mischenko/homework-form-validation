import React, { Component } from 'react';

export class TextareaComponent extends Component {
    constructor(props) {
        super(props);
        
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        console.log('InputTextComponent '+ this.props.name +' value changed');

        const inputValue = event.target.value;

        this.props.onChange(this.props.name, inputValue.toUpperCase());// calls parent event handler with component own name and inputValue
    }

    render() {
        return (
            <div>
                <label>
                {this.props.label}:<br></br>
                <textarea value={this.props.value} onChange={this.handleChange} />       
                </label>
            </div>
        );
    }
}