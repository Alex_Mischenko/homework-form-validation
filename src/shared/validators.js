export function isEmail(input) {
    let emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
    let valid = emailRegEx.test(input);
    return valid;
}

export function isPassword(input) {
    let hasNumbers = /[0-9]/;
    let hasLowerCase = /[a-z]/;
    let hasUpperCase = /[A-Z]/;

    let valid = false;
    if (hasNumbers.test(input) && hasLowerCase.test(input) && hasUpperCase.test(input)) valid = true;

    return valid;
}