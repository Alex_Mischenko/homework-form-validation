import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import { FormComponent } from './components/FormComponent';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        
        <FormComponent title="Registration form" />
        
      </div>
    );
  }
}

export default App;
